var http = require('http')
var bl = require('bl')

var results = []
var count = 0

function logResult(){
  for(var i = 0; i < 3; i++){
    console.log(results[i]);
  }
}

function doget(index){
  http.get(process.argv[2 + index], function(result){
    result.pipe(bl(function(err, data){
      if(err){return console.error(err)}
      results[index] = data.toString()
      count++

      if(count == 3){
        logResult()
      }
    }))
  })
}

for(var i = 0; i < 3; i++){
  doget(i)
}
