var map = require('through2-map'),
    http = require('http');

var server = http.createServer(function(req, res){
  ig(req.method != 'POST')
    return res.end('send me a POST\n')
  req.pipe(map(function(chunk){
    return chunk.toString().toUpperCase()
  })).pipe(res)
}).listen(Number(process.argv[2])
