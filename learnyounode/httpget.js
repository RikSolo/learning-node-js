var http = require('http')
var url = process.argv[2]
function doget(url, callback){
  http.get(url, function(response){
    response.on("data", function(data){
      return callback(data.toString())
    })
    response.on("error", function(error){
      return callback(error.toString())
    })
  })
}
function logResult(data){
  console.log(data)
}

doget(url, logResult)
